package com.mindgame.wordfi.resources;

/**
 * @author Rodrigue Ngalani Touko
 * check the direction of the inserted word
 */
public enum WordDirection {
    Horizontal,
    Vertical;
}
