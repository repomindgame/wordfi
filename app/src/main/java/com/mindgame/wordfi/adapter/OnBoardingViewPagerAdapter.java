package com.mindgame.wordfi.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mindgame.wordfi.R;
import com.mindgame.wordfi.model.OnBoardingItem;

import java.util.List;

public class OnBoardingViewPagerAdapter extends RecyclerView.Adapter<OnBoardingViewPagerAdapter.OnBoardingViewHolder> {

    private  List<OnBoardingItem> onBoardingItems;

    public OnBoardingViewPagerAdapter(List<OnBoardingItem> onBoardingItems) {
        this.onBoardingItems = onBoardingItems;
    }

    @NonNull
    @Override
    public OnBoardingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OnBoardingViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_container_onboarding, parent, false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull OnBoardingViewHolder holder, int position) {
        holder.setOnBoardingData(onBoardingItems.get(position));
    }

    @Override
    public int getItemCount() {
        return onBoardingItems.size();
    }

    static class  OnBoardingViewHolder extends RecyclerView.ViewHolder{

        final ImageView onBoardingImage;
        final TextView onBoardingDescription;

         OnBoardingViewHolder(@NonNull View itemView) {
            super(itemView);
            onBoardingImage = itemView.findViewById(R.id.onBoardingImageView);
            onBoardingDescription = itemView.findViewById(R.id.onBoardingTextDescription);
        }

        void setOnBoardingData(OnBoardingItem onBoardingItem){
             onBoardingImage.setImageResource(onBoardingItem.getScreenImage());
             onBoardingDescription.setText(onBoardingItem.getDescription());
        }
    }
}
