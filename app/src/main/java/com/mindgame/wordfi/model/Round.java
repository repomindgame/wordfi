package com.mindgame.wordfi.model;

import com.mindgame.wordfi.resources.RoundMode;

import java.util.ArrayList;
import java.util.List;

/***
 * @author Rodrigue Ngalani Touko
 *
 */
public class Round {

    final char[] characters;
    private int score;


    public Round(List<String> words, char[] characters) {
        this.characters = characters;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

}
