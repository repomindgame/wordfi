package com.mindgame.wordfi.model;

import com.mindgame.wordfi.resources.WordDirection;

/***
 * @author Rodrigue Ngalani Touko
 *
 *Word Attributes specifications
 */
public class Word  {
    final String word;
    final int length;
    final boolean isFound;

    public Word(String word,  boolean isFound) {
        this.word = word;
        this.length = word.length();
        this.isFound = isFound;
    }

}
