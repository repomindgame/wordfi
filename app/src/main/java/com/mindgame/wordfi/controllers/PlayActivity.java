package com.mindgame.wordfi.controllers;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.mindgame.wordfi.R;
import com.mindgame.wordfi.services.Settings;
import com.mindgame.wordfi.services.WordProvider;
import com.mindgame.wordfi.widgets.LettersBoard;
import com.mindgame.wordfi.services.Screen;
import com.mindgame.wordfi.widgets.Puzzle;
import com.mindgame.wordfi.widgets.Solver;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/***
 * @author Rodrigue Ngalani Touko
 *Play Activity
 */
public class PlayActivity extends AppCompatActivity {

    private Solver solver;
    private LettersBoard board;
    private MaterialButton helpButton;
    private TextView timeCounterState, selectedText;
    private Timer timer;
    private Double time = 0.0;
    private int helpCounter = 0;
    private String[] letters = new String[6];
    private List<String> words = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Screen(this).fullMode();
        setContentView(R.layout.activity_play);

        loadPuzzleResources();

        LinearLayout puzzleBoard = findViewById(R.id.puzzle_grid_board);
        MaterialButton leaveButton = findViewById(R.id.leave_button);
        helpButton = findViewById(R.id.help_button);
        TextView answerCheckButton = findViewById(R.id.check_answer_button);
        timeCounterState = findViewById(R.id.timer_state);
        selectedText = findViewById(R.id.selected_letters);
        TextView firstLetter = findViewById(R.id.letter_item_1);
        TextView secondLetter = findViewById(R.id.letter_item_2);
        TextView thirdLetter = findViewById(R.id.letter_item_3);
        TextView fourthLetter = findViewById(R.id.letter_item_4);
        TextView fifthLetter = findViewById(R.id.letter_item_5);
        TextView sixthLetter = findViewById(R.id.letter_item_6);

        Puzzle puzzle = new Puzzle(this, puzzleBoard, words);
        puzzle.create(words);
        solver = new Solver(this, puzzle);
        timer = new Timer();
        board = new LettersBoard(firstLetter, secondLetter, thirdLetter, fourthLetter, fifthLetter, sixthLetter, selectedText );
        board.load(letters);
        leaveButton.setOnClickListener(v -> {
            Intent intent=new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });
        answerCheckButton.setOnClickListener(view -> {
            solver.search(selectedText.getText().toString());
            board.reset();
        });
        helpButton.setOnClickListener(v -> puzzleHelp());
        startTimer();
    }

    private void loadPuzzleResources(){
        WordProvider wordProvider = new WordProvider(this);
        words = wordProvider.getPuzzleWords();
        letters = wordProvider.getBoardLetters();
    }

    /**
     * start a timer to track elapsed time
     */
    private void startTimer()
    {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    time++;
                    timeCounterState.setText(getTimerText());
                });
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0 ,1000);
    }

    /**
     * playing time tracking
     * @return String of the elapsed time
     */
    private @NotNull String getTimerText()
    {
        int rounded = (int) Math.round(time);
        int seconds = ((rounded % 86400) % 3600) % 60;
        int minutes = ((rounded % 86400) % 3600) / 60;
        int hours = ((rounded % 86400) / 3600);
        return formatTime(seconds, minutes, hours);
    }

    /**
     * @param seconds  elapsed seconds
     * @param minutes  elapsed minutes
     * @param hours  elapsed hours
     * @return Parameters in Time Format
     */
    private @NotNull String formatTime(int seconds, int minutes, int hours)
    {
        final String format = String.format("%s : %s : %s",
                String.format("%02d", hours),
                String.format("%02d", minutes),
                String.format("%02d", seconds));
        return format;
    }

    /**
     *get help from System to
     * show some Letters on the Puzzle
     */
    private void puzzleHelp(){
        if (helpCounter < helpLength()){
            solver.help();
            helpCounter++;
            if(helpCounter == helpLength()){
                ((ViewGroup) helpButton.getParent()).removeView(helpButton);
            }
        }
    }

    /**
     *check the Level to allow a maximum
     * of help possibility
     * @return number of possible help
     */
    private int helpLength(){
        String levelMode = Settings.getLevel(this);
        String[] levels = this.getResources().getStringArray(R.array.level);
        if(levelMode.equals(levels[1])){
            return 2;
        }else if(levelMode.equals(levels[2])){
            return 3;
        }else if(levelMode.equals(levels[3])){
            return 4;
        }else{
            return 1;
        }
    }
}