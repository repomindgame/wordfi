package com.mindgame.wordfi.services;

import android.media.MediaPlayer;

import com.mindgame.wordfi.R;
import com.mindgame.wordfi.controllers.WelcomeActivity;

import static com.mindgame.wordfi.controllers.WelcomeActivity.mediaPlayer;

public class SoundManager {

    /**
     * start the background music
     */
    public static void startBackgroundMusic(){
        mediaPlayer =  MediaPlayer.create(WelcomeActivity.context, R.raw.ukulele);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    /**
     * stop the background Music
     */
    public static void stopBackgroundMusic(){
        mediaPlayer.stop();
    }

    /**
     * change the background music state to Mute
     */
    public static void mute(){
        mediaPlayer.setVolume(0f,0f);
    }

    /**
     * change the volume of the music in background
     * @param params value of the selected volume
     */
    public static void setVolumeTo(int params){
        float value = params / 100f;
        mediaPlayer.setVolume(value,value);
    }

    /**
     * check if the mediaPlayer is playing
     * @return the state of mediaPlayer
     */
    public static boolean isPlaying(){
      return mediaPlayer.isPlaying();
    }


}
