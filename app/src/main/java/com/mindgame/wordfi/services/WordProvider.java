package com.mindgame.wordfi.services;

import android.content.Context;

import com.mindgame.wordfi.resources.Dico;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Provide List of Word to fill in Puzzle
 * and Letters to fill in Board
 */
public class WordProvider {
        private final Context context;
        private  Dico dico;

    public WordProvider(Context context) {
        this.context = context;
        dico = new Dico(context);
    }


    public List<String> getPuzzleWords(){
        return anagramWords();
    }

    public String[] getBoardLetters(){
        String longestWord = null;
        int maxLength = 0;
        for (String s : anagramWords()) {
            if (s.length() > maxLength) {
                maxLength = s.length();
                longestWord = s;
            }
        }
        return longestWord.split("");
    }


    private  List<String> anagramWords()
    {
        List<String> wordsList = dico.puzzlesWords();
        HashMap<String, List<String> > map = new HashMap<>();
        List<String> puzzleWords = new ArrayList<>();
        // loop over all words
        for (int i = 0; i < wordsList.size(); i++) {

            String word = wordsList.get(i);
            char[] letters = word.toCharArray();
            Arrays.sort(letters);
            String newWord = new String(letters);

            if (map.containsKey(newWord)) {
                map.get(newWord).add(word);
            }
            else {
                List<String> words = new ArrayList<>();
                words.add(word);
                map.put(newWord, words);
            }
        }
        for (String s : map.keySet()) {
            List<String> values = map.get(s);
            if (values.size() > 1) {
                puzzleWords = values;
            }
        }
        return puzzleWords;
    }


}
