package com.mindgame.wordfi.services;

import android.app.Activity;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

/**
     * @author Rodrigue Touko
     *get and change screen parametters
 */
public class Screen {

    private final Activity activity;

    public Screen(Activity activity) {
            this.activity = activity;
        }

    /**
     * Change the current Activity Screen Mode to Full Mode
     */
    public void fullMode() {
            int flags = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN;
                flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            activity.getWindow().getDecorView().setSystemUiVisibility(flags);
        }

    /**
     * get the size With of the current Screen
     * @return screen With size
     */
    public float getWith(){
            Resources resources = activity.getResources();
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            return displayMetrics.widthPixels;
    }
}