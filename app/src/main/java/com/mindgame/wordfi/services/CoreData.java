package com.mindgame.wordfi.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;


public class CoreData {
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;
    private final String onBoardingStateKey;
    private final String highScoreKey;
    private final String themeKey;
    private final String languageKey;
    private final String levelKey;
    private final String soundKey;
    private final String volumeKey;
    private final String bestTime;

    /***
     * @author Rodrigue Ngalani Touko
     * Local Database to Store User Game Preferences
     */
    @SuppressLint("CommitPrefEdits")
    public CoreData(Context context) {
        this.sharedPreferences = context.getSharedPreferences("App_Preferences",Context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
        onBoardingStateKey = "onBoardingState";
        highScoreKey = "highScore";
        themeKey = "theme";
        languageKey = "language";
        levelKey = "level";
        soundKey = "sound";
        volumeKey = "volume";
        bestTime = "bestTime";
    }

    /**
     * save that the user have see On boarding
     * @param value state after first start
     */
    public void saveOnBoardingState(Boolean value){
        editor.putBoolean(onBoardingStateKey,value);
        editor.commit();
    }

    /**
     * Check if the User have already see the On boarding
     * @return true if on boarding was already performed
     */
    public Boolean getOnBoardingState(){
        return sharedPreferences.getBoolean(onBoardingStateKey,false);
    }

    /**
     * save the HighScore
     * @param value new score
     */
    public void saveHighScore(long value){
        editor.putLong(highScoreKey,value);
        editor.commit();
    }

    /**
     * retrieve the current HighScore
     * @return current HighScore
     */
    public long getHighScore(){
        return sharedPreferences.getLong(highScoreKey,00);
    }

    /**
     * save a new Theme Preference
     * @param value theme index
     */
    public void saveTheme(int value){
        editor.putInt(themeKey,value);
        editor.commit();
    }

    /**
     * retrieve the current theme index
     * @return theme index
     */
    public int getTheme(){
        return sharedPreferences.getInt(themeKey,0);
    }

    /**
     * save a new language Preference
     * @param value language index
     */
    public void saveLanguage(int value){
        editor.putInt(languageKey,value);
        editor.commit();
    }

    /**
     * retrieve the current language index
     * @return current language index
     */
    public int getLanguage(){
        return sharedPreferences.getInt(languageKey,3);
    }

    /**
     * save a new Level Preference
     * @param value level index
     */
    public void saveLevel(int value){
        editor.putInt(levelKey,value);
        editor.commit();
    }

    /**
     * retrieve the current level index
     * @return level index
     */
    public int getLevel(){
        return sharedPreferences.getInt(levelKey,0);
    }

    /**
     * save a new sound state (Mute/On) preference
     * @param state selected preference
     */
    public void saveSoundState(Boolean state){
        editor.putBoolean(soundKey,state);
        editor.commit();
    }

    /**
     * retrieve the current sound State Preference
     * @return true if the sound is On
     */
    public Boolean getSoundState(){
        return  sharedPreferences.getBoolean(soundKey,true);
    }

    /**
     * save a new volume value
     * @param value selected volume value
     */
    public void saveVolume(int value){
        editor.putInt(volumeKey,value);
        editor.commit();
    }

    /**
     * retrieve the current volume value
     * @return current volume value
     */
    public int getVolume(){
        return sharedPreferences.getInt(volumeKey,50);
    }

    /**
     * save the best record of finished Game in selected Level
     * @param bestTime new record
     */
    public void saveBestTime(String bestTime){
        editor.putString(volumeKey,bestTime);
        editor.commit();
    }

    /**
     * retrieve the current record of finished Round in selected level
     * @return current record value
     */
    public String getBestTime(){
        return sharedPreferences.getString(bestTime,"00:00:00");
    }

}
