package com.mindgame.wordfi.widgets;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindgame.wordfi.R;
import com.mindgame.wordfi.resources.WordDirection;
import com.mindgame.wordfi.services.Screen;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 *@author  Rodrigue Ngalani Touko
 * this Class build the Puzzle and fill the given words
 */
public class Puzzle {

    private final LinearLayout puzzleBoard;
    private final int boardLength;
    private final TextView[][] grid;
    private final Drawable[] cellDesign;
    private final Activity activity;
    private final float screenWith;
    private final int wordListLength;


    public Puzzle(Activity activity, LinearLayout puzzleBoard, @NotNull List<String> words){
        this.puzzleBoard = puzzleBoard;
        this.activity = activity;
        this.boardLength =  8;
        this.grid = new TextView[boardLength][boardLength];
        this.cellDesign = new Drawable[4];
        this.screenWith = new Screen(activity).getWith();
        this.wordListLength = words.size();
    }

    /**
     * Getter of the current grid
     * @return the current grid
     */
    public TextView[][] getGrid() {
        return grid;
    }

    /**
     *initialise the puzzle
     */
    private void initialize(){
        loadResources();
        int cellSize = Math.round(screenWith/boardLength);
        int letterSize = cellSize / 5;
        LinearLayout.LayoutParams layoutParamsRow = new LinearLayout.LayoutParams(cellSize * boardLength, cellSize);
        LinearLayout.LayoutParams layoutParamsCell = new LinearLayout.LayoutParams(cellSize,cellSize);
        for (int i = 0; i < boardLength; i++){
            LinearLayout linearRow = new LinearLayout(activity);
            for(int j = 0; j < boardLength; j++) {
                grid[i][j] = new TextView(activity);
                grid[i][j].setText(" ");
                grid[i][j].setBackground(cellDesign[0]);
                grid[i][j].setTextColor(activity.getResources().getColor(R.color.transparentColor));
                grid[i][j].setGravity(Gravity.CENTER);
                grid[i][j].setTextSize(letterSize);
                linearRow.addView(grid[i][j],layoutParamsCell);
            }
            puzzleBoard.addView(linearRow,layoutParamsRow);
        }
    }

    /**
     *load different type of Puzzle Cell forms and Colors
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    private void loadResources() {
        cellDesign[0] = activity.getResources().getDrawable(R.color.transparentColor);// emptyLetters
        cellDesign[1] = activity.getResources().getDrawable(R.drawable.puzzle_cell);// hiddenLetters
        cellDesign[2] = activity.getResources().getDrawable(R.drawable.beginner_level); // foundByPlayer
        cellDesign[3] = activity.getResources().getDrawable(R.drawable.puzzle_cell);// foundByFriend
    }

    /**
     * insert the first word on the Puzzle
     * @param word selected word
     */
    private  void insertFirstWord(@NotNull String word){
        int row = (grid.length - 1) / 2;
        int col = (grid.length - word.length()) / 2;

        for(int i = 0; i < word.length() ; i++){
            grid[row][col].setText(String.valueOf(word.charAt(i)));
            grid[row][col].setBackground(cellDesign[1]);
            col = col + 1;
        }
    }

    /**
     * check if a [x][y] point is on the Puzzle
     * @param i point on x
     * @param j point on x
     * @return  true if the point is on the Puzzle
     */
    private @NotNull Boolean isValid(int i, int j){
        return i >= 0 && i < grid.length && j >= 0 && j < grid.length;
    }

    /**
     *
     * @param fix start point direction (Column or Row)
     * @param start start point (Column or Row)
     * @param end end point of the given word
     * @param direction direction to insert the word
     * @param word given word
     */
    private  void insertWord(int fix, int start, int end, WordDirection direction, String word){
        if(direction == WordDirection.Horizontal){
            int index = 0;
            for(int col = start; col < end + 1; col ++){
                grid[fix][col].setText(String.valueOf(word.charAt(index)));
                grid[fix][col].setBackground(cellDesign[1]);
                index = index + 1;
            }
        }
        if(direction == WordDirection.Vertical){
            int index = 0;
            for(int row = start; row < end + 1; row++){
                grid[row][fix].setText(String.valueOf(word.charAt(index)));
                grid[row][fix].setBackground(cellDesign[1]);
                index = index + 1;
            }
        }
    }

    /**
     *search the perfect place for the given word
     * @param word word that have to be insert on the Puzzle
     */
    private  void searchPlaceFor(@NotNull String word) {
        // for all possibility
        for(int i = 0; i < word.length(); i++){
            for(int row = 0; row < grid.length; row++){
                for (int col = 0; col < grid.length; col++) {
                    if(word.charAt(i) == grid[row][col].getText().charAt(0)){
                        int hStart = col - i;
                        int hEnd = col + (word.length() - (i + 1));
                        int vStart = row - i;
                        int vEnd = row + (word.length() - (i + 1));
                        // check if the case before the first letter of the word is outOfGrid
                        if(!isValid(row,hStart - 1) || !isValid(row,hEnd + 1) ){
                            // try horizontal insertion
                            if(     isValid(row, col - 1)  && isValid(row, col + 1) && grid[row][col - 1].getText() == " " && grid[row][col + 1].getText() == " "  && isAdjacent(row,col,i,WordDirection.Horizontal,word)
                                    || isValid(row, col - 1)  && isValid(row, col + 1) && grid[row][col - 1] == null && grid[row][col + 1].getText() == " "  && isAdjacent(row,col,i,WordDirection.Horizontal,word)
                                    || isValid(row, col - 1)  && isValid(row, col + 1) && grid[row][col - 1].getText() == " " && grid[row][col + 1] == null  && isAdjacent(row,col,i,WordDirection.Horizontal,word)){
                                int after = word.length() - (i + 1);
                                if(isValid(row, col - i) && isValid(row, col + after)){
                                    insertWord(row, col - i, col + after, WordDirection.Horizontal, word);
                                    return;
                                }

                            }
                        }else if( isValid(row,hStart - 1) && isValid(row,hEnd + 1) && grid[row][hStart - 1].getText() == " " && grid[row][hEnd +  1].getText() == " "){
                            if(     isValid(row, col - 1)  && isValid(row, col + 1) && grid[row][col - 1].getText() == " " && grid[row][col + 1].getText() == " "  && isAdjacent(row,col,i,WordDirection.Horizontal,word)
                                    || isValid(row, col - 1)  && isValid(row, col + 1) && grid[row][col - 1] == null && grid[row][col + 1].getText() == " "  && isAdjacent(row,col,i,WordDirection.Horizontal,word)
                                    || isValid(row, col - 1)  && isValid(row, col + 1) && grid[row][col - 1].getText() == " " && grid[row][col + 1] == null  && isAdjacent(row,col,i,WordDirection.Horizontal,word)){
                                int after = word.length() - (i + 1);
                                if(isValid(row, col - i) && isValid(row, col + after)){
                                    insertWord(row, col - i, col + after, WordDirection.Horizontal, word);
                                    return;
                                }

                            }
                        }
                        // check if the case before the first letter of the word is outOfGrid
                        if (!isValid(vStart - 1,col) || !isValid(vEnd + 1,col)){
                            // try vertical insertion
                            if(     isValid(row - 1, col) && isValid(row + 1, col) && grid[row - 1][col].getText() == " " && grid[row + 1][col].getText() == " " && isAdjacent(row,col,i,WordDirection.Vertical,word)
                                    || isValid(row - 1, col) && isValid(row + 1, col) && grid[row - 1][col] == null && grid[row + 1][col].getText() == " " && isAdjacent(row,col,i,WordDirection.Vertical,word)
                                    || isValid(row - 1, col) && isValid(row + 1, col) && grid[row - 1][col].getText() == " " && grid[row + 1][col] == null && isAdjacent(row,col,i,WordDirection.Vertical,word)){
                                int after = word.length() - (i + 1);
                                if(isValid(row - i, col) && isValid(row + after, col)){
                                    insertWord(col, row - i, row + after, WordDirection.Vertical, word);
                                    return;
                                }
                            }
                        }else if( isValid(vStart - 1,col) && isValid(vEnd + 1,col) && grid[vEnd + 1][col].getText() == " " && grid[vStart - 1][col].getText()  == " "){
                            if(     isValid(row - 1, col) && isValid(row + 1, col) && grid[row - 1][col].getText() == " " && grid[row + 1][col].getText() == " " && isAdjacent(row,col,i,WordDirection.Vertical,word)
                                    || isValid(row - 1, col) && isValid(row + 1, col) && grid[row - 1][col] == null && grid[row + 1][col].getText() == " " && isAdjacent(row,col,i,WordDirection.Vertical,word)
                                    || isValid(row - 1, col) && isValid(row + 1, col) && grid[row - 1][col].getText() == " " && grid[row + 1][col] == null && isAdjacent(row,col,i,WordDirection.Vertical,word)){
                                int after = word.length() - (i + 1);
                                if(isValid(row - i, col) && isValid(row + after, col)){
                                    insertWord(col, row - i, row + after, WordDirection.Vertical, word);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param row Row start Point
     * @param col Column start Point
     * @param index index of the interception letter
     * @param direction direction of insertion
     * @param word given word
     * @return return true if the word can fit on the Grid
     */
    private boolean isAdjacent(int row, int col, int index, WordDirection direction, String word){
        if(direction == WordDirection.Horizontal){
            int start = col - index;
            int length = 0;
            for (int i = 0; i < word.length(); i++) {
                if (    isValid(row+1,start+i) && isValid(row-1,start+i)  && (start + i) != index  &&  grid[row - 1][start + i].getText() == " " && grid[row + 1][start + i].getText() == " "
                    ||  isValid(row+1,start+i) && isValid(row-1,start+i)  && (start + i) != index   && grid[row - 1][start + i].getText() == " " && grid[row + 1][start + i] == null
                    ||  isValid(row+1,start+i) && isValid(row-1,start+i)  && (start + i) != index   && grid[row - 1][start + i] == null && grid[row + 1][start + i].getText() == " "
                    ||  isValid(row+1,start+i) && isValid(row-1,start+i)  && (start + i) != index   && grid[row - 1][start + i] == null && grid[row + 1][start + i] == null) {
                    length++;
                    if (length+1 == word.length()){
                        return true;
                    }
                }
            }
        }else if(direction == WordDirection.Vertical){
            int start = row - index;
            int length = 0;
            for (int i = 0; i < word.length(); i++) {
                if (    isValid(start+i,col+1) && isValid(start+i,col-1)  && (start + i) != index && grid[start + i][col-1].getText() == " " && grid[start+i][col+1].getText() == " "
                    ||  isValid(start+i,col+1) && isValid(start+i,col-1)  && (start + i) != index && grid[start + i][col-1].getText() == " " && grid[start+i][col+1] == null
                    ||  isValid(start+i,col+1) && isValid(start+i,col-1)  && (start + i) != index && grid[start + i][col-1] == null && grid[start+i][col+1].getText() == " "
                    ||  isValid(start+i,col+1) && isValid(start+i,col-1)  && (start + i) != index && grid[start + i][col-1] == null && grid[start+i][col+1] == null) {
                    length++;
                    if (length+1 == word.length()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * sort words in the given list
     * @param words list of word to be insert on the puzzle
     * @return return a sorted list of word  by length
     */
    private static @NotNull List<String> sortWordsByLength(@NotNull List<String> words) {
        List<String> sortedWords = new ArrayList<>();
        int length = words.size();

        for(int i = 0; i < length; i++) {
            String longestWord = "";
            for (String word : words) {
                if(word.length() > longestWord.length()) {
                    longestWord = word;
                }
            }
            sortedWords.add(longestWord);
            words.remove(longestWord);
        }
        return sortedWords;
    }

    /**
     *  create the puzzle
     * @param words list of word to be insert on the puzzle
     */
    public void create(List<String> words){
        initialize();
        List<String> sortedWords = sortWordsByLength(words);
        boolean isFirstWord = true;
        for(int i = 0; i < wordListLength; i++){
            if(isFirstWord)
            {
                isFirstWord = false;
                insertFirstWord(sortedWords.get(i));
            }
            else {
                    searchPlaceFor(sortedWords.get(i));
            }
        }
    }
}