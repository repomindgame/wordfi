package com.mindgame.wordfi.widgets;


import android.app.Activity;
import android.content.res.ColorStateList;
import android.widget.TextView;

import com.mindgame.wordfi.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/***
 * @author Rodrigue Ngalani Touko
 * This Class is to Solve the Puzzle
 */
public class Solver {

    private final TextView[][] grid;
    private final Activity activity;
    private long score = 0;
    private final TextView scoreState;

    /**
     * Puzzle Constructor
     * @param activity the current activity
     * @param puzzle the puzzle where words have to be found
     */
    public Solver(Activity activity, Puzzle puzzle) {
        this.activity = activity;
        this.grid = puzzle.getGrid();
        scoreState = activity.findViewById(R.id.score_state);
        setScore(score);
    }

    /**
     * check if a [x][y] point is on the Puzzle
     * @param i point on x
     * @param j point on x
     * @return  true if the point is on the Puzzle
     */
    private @NotNull Boolean isValid(int i, int j){
        return i >= 0 && i < grid.length && j >= 0 && j < grid.length;
    }

    /**
     * search the entered word
     * @param word word that have to be searched in the Puzzle
     */
    public void search(String word) {
        if(word.length() >= 3 ){
            for (int row = 0; row < grid.length; row++) {
                for (int col = 0; col < grid.length; col++) {
                    if (grid[row][col].getText().charAt(0) == word.charAt(0)) {
                        if(isHorizontalWord(row,col,word)){
                            setScore(word.length());
                        }else if(isVerticalWord(row,col,word)){
                            setScore(word.length());
                        }
                    }
                }
            }
        }

    }

    /**
     * Horizontal search of the given word
     * @param row define the line of the first letter of the given word
     * @param col define the column of the first letter of the given word
     * @param word word that have to be searched in the Puzzle
     * @return true if the word have been found on Horizontal direction
     */
    private boolean isHorizontalWord(int row, int col,  String word){
         int length = 0;
         int start;
         int end = col + (word.length() - 1);
        if (isValid(row,end)){
            for (int i = 0; i < word.length(); i++) {
                if(isValid(row,col + i) && grid[row][col + i].getText().charAt(0) == word.charAt(i)) {
                    length++;
                    start = row;
                    if(length == word.length() && grid[row][end + 1].getText() == " "
                        || length == word.length() && !isValid(row,end+1)){
                        for (int j = 0; j < word.length(); j++) {
                            grid[start][col + j].setTextColor(activity.getColor(R.color.error_color));
                        }
                        return true;
                    }
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Vertical search of the given word
     * @param row define the line of the first letter of the given word
     * @param col define the column of the first letter of the given word
     * @param word word that have to be searched in the Puzzle
     * @return true if the word have been found on Vertical direction
     */
    private boolean isVerticalWord(int row, int col,  String word){
        int length = 0;
        int start;
        int end = row + (word.length() - 1);
        if (isValid(end,col)){
            for (int i = 0; i < word.length(); i++) {
                if(isValid(row + i,col) && grid[row + i][col].getText().charAt(0) == word.charAt(i)) {
                    length++;
                    start = col;
                    if(length == word.length() && !isValid(end+1,col)
                        ||length == word.length() && grid[end + 1][col].getText() == " "){
                        for (int j = 0; j < word.length(); j++) {
                            grid[row + j][start].setTextColor(activity.getColor(R.color.error_color));
                        }
                        return true;
                    }
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * increase the current Score
     * @param value value of the point to be added on current Score
     */
    private void setScore(long value){
         score = score + (value * 100);
        scoreState.setText(String.valueOf(score));
     }

    /**
     * show a hidden letter on the Puzzle to help the User
     */
    public void help(){
         boolean isDone = false;
         ColorStateList filledCaseColor = activity.getColorStateList(R.color.error_color);
        for (TextView[] textViews : grid) {
            for (int j = 0; j < grid.length; j++) {
                if (textViews[j].getText() != " " && textViews[j].getTextColors() != filledCaseColor && !isDone) {
                    textViews[j].setTextColor(activity.getColor(R.color.error_color));
                    isDone = true;
                }
            }
        }
    }


    private void completedRound(){
        // show a Pop Up with the Round State
    }

}
